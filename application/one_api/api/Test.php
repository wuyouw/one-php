<?php
namespace app\one_api\api;
use app\one_api\api\ApiInit;
use app\common\model\SystemAnnex as AnnexModel;


class Test extends ApiInit
{
    public function initialize() {
        parent::initialize();
    }
    
    public function index() {
        $this->_success('msg',['a'=>1]);
    }

    public function upload($from = 'input', $group = 'sys', $water = '', $thumb = '', $thumb_type = '', $input = 'file')
    {
        return json(AnnexModel::upload($from, $group, $water, $thumb, $thumb_type, $input));
    }

    public function test(){
        var_dump('abc');
    }
}
