<?php


namespace app\pay\home;

use app\common\controller\Common;
use Env;

/**
 * 支付测试
 * 为了安全起见，测试完请删除或者禁用此文件
 * @package app\pay\home
 */
class Test extends Common
{
    protected function initialize()
    {
        parent::initialize();
        //die('如需测试，请手动注释此行，为了安全，测试完记得取消注释');
    }

    public function index()
    {
        echo '支付成功';
    }

    /**
     * [示例]发起支付
     */
    public function paySubmit()
    {
        $orderNo = input('get.order_no', order_number());
        $method = input('get.method', 'wechat_qr');
        $param = [];
        $param['method'] = $method; // 支付方式[wechat, alipay]
        $param['money'] = input('get.total', 6); // 支付金额
        $param['uid'] = 1000001; // 用户标识
        $param['order_no'] = $orderNo; // 订单号
        $param['body'] = '支付测试'; // 支付标题
        $param['goback'] = get_domain() . '/pay/test'; // 同步回调后的跳转地址
        $param['openid'] = '';
        $param['sign'] = md5(http_build_query($param) . config('one_auth.key'));



        if ($method == 'wechat_app') {
            $rs = action('pay/index/wx_app', [$param], 'home');
            exit(json_encode($rs));
        }
        if ($method == 'alipay_app') {
            $param['product_code'] = 'QUICK_MSECURITY_PAY';
            $param['pay_method'] = 'alipay.trade.app.pay';
            $rs = action('pay/index/wx_app', [$param], 'home');
            exit(json_encode($rs));
        }

        $this->redirect('/pay?' . http_build_query($param));
    }

    /**
     * [示例]发起退款
     */
    public function refundSubmit()
    {
        $orderNo = input('get.order_no');

        $param = [];
        $param['order_no'] = $orderNo; // 已支付的订单号
        $param['money'] = 0.01; // 支付金额
        $param['goback'] = get_domain() . '/pay/test'; // 同步回调后的跳转地址
        $param['sign'] = md5(http_build_query($param) . config('one_auth.key'));
        $this->redirect('/pay/index/refund?' . http_build_query($param));
    }

    /**
     * [示例]支付回调
     * @param int $orderNo 订单号
     * @param array $params 所有回传参数
     */
    public function payment($orderNo, $param = [])
    {
        if (!defined('IS_SAFE_PAY')) {
            return ['status' => false, 'message' => '非法请求'];
        }
        // 订单业务处理代码
        file_put_contents(Env::get('runtime_path') . $orderNo . '_payment.txt', json_encode($param, 1));
        return ['status' => true, 'url' => '/pay/test'];
    }

    /**
     * [示例]退款回调
     * @param int $orderNo 订单号
     * @param array $params 所有回传参数
     */
    public function refund($orderNo, $param = [])
    {
        if (!defined('IS_SAFE_PAY')) {
            return ['status' => false, 'message' => '非法请求'];
        }
        // 订单业务处理代码
        file_put_contents(Env::get('runtime_path') . $orderNo . '_refund.txt', json_encode($param, 1));
        return ['status' => true, 'url' => '/pay/test'];
    }
}
