<?php
namespace app\index\home;

class Index
{
    public function index()
    {
        return '<link rel="stylesheet" href="static/css/flex.css"><link href="static/lib/mdi-font@5.5.55/css/materialdesignicons.min.css" media="all" rel="stylesheet" type="text/css" /><style>div>i:hover{color:#00C250;transform: rotateY(180deg);}</style><div style="height:100%" flex="main:center dir:top cross:center wrap:wrap"><i style="font-size:150px;" class="mdi mdi-airport"></i><div style="font-size:36px;">这是留给你自由发挥的地方^_^!</div></div>';
    }

    public function hello($name = 'ThinkPHP5')
    {
        return 'hello,' . $name;
    }
}
