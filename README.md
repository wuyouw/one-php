# one-php

#### 介绍

vue2+elemnet+thinkphp5.1 后端渲染基础框架

#### 基础功能

- 基础权限角色管理

- 动态配置管理

- 数据库管理

- 日志管理

- API接口管理

- 支付管理

#### 软件架构

- ThinkPHP V5.1.27 5.1版本已经正式作为LTS版本，提供长达三年的服务支持，可以放心使用。
- vue@2.6.12
- element-ui@2.14.1
- mdi-font@5.5.55 内置5K+高质量图标

#### 安装教程

1.  将运行目录指向`public目录`
2.  访问域名进行初始化安安装

![输入图片说明](https://images.gitee.com/uploads/images/2020/1212/205230_c4141d49_60481.png "屏幕截图.png")

#### 部分页面载图

![输入图片说明](https://images.gitee.com/uploads/images/2020/1212/205740_553bdc35_60481.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/1212/205818_7a6507eb_60481.png "屏幕截图.png")