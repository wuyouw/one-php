<?php

// [ API入口 ]
namespace think;

header('Content-Type:application/json');
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods:GET,POST");
header("Access-Control-Allow-Headers: token,secret,timestamp");
if(strtoupper($_SERVER['REQUEST_METHOD'])== 'OPTIONS') exit('Connected!');
// 检测PHP环境
if(version_compare(PHP_VERSION,'5.6.0','<'))  die('PHP版本过低，最少需要PHP5.6，请升级PHP版本！');

// 定义应用目录
define('APP_PATH', __DIR__ . '/application/');

// 定义入口
define('ENTRANCE','api');
define('DOC_ROOT', __DIR__ );
// 加载基础文件
require __DIR__ . '/../thinkphp/base.php';

Container::get('app')->run()->send();